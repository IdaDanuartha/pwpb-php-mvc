<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="row my-3">
        <div class="col-lg-12">
            <?php Flasher::flash(); ?>
        </div>
    </div>
    <h2 class="mb-3">Post Page</h2>
    <table class="table table-hover">
    <thead>
        <tr class="table-dark">
        <th scope="col">#</th>
        <th scope="col">First</th>
        <th scope="col">Last</th>
        <th scope="col">Handle</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
        </tr>
        <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
        </tr>
        <tr>
        </tr>
    </tbody>
    </table>
</main>